\section{Description des objectifs de la recherche}
\label{Chap1Seq2QHYPG}

\subsection{Problématique}

Le problème de la localisation coopérative en robotique mobile est un sujet qui intéresse de plus en plus de chercheurs. Habituellement, ce problème est résolu dans un environnement fermé de dimensions fixes. On utilise des caméras ou des capteurs lasers très performants, généralement lourds et très coûteux, en combinaison avec des marqueurs placés dans l'environnement. Il devient alors réalisable de localiser un ou plusieurs robots dans ce type d'environnement contrôlé grâce à différentes techniques de filtrage. Comme cela a été montré à la section~\ref{Chap1Sec1SOTA}, les chercheurs ont poussé la recherche en robotique afin de rendre une équipe de robots un peu plus autonome dans un environnement inconnu. Différentes techniques de localisation relative en temps réel ont été conçues et celles-ci ont une précision et une efficacité toujours croissantes. Cependant, la démocratisation de plus en plus rapide de la robotique nous amène à faire face à des problèmes de localisation beaucoup plus contraignants, que ce soit au niveau des coûts, des dimensions ou du poids total de l'instrumentation utilisée. 

Les tryphons sont des robots dirigeables volants de forme cubique conçus par St-Onge, Gosselin et Reeves~\cite{st2010dynamic}. Ils ont été créés dans le cadre d'un projet artistique permettant de faire des spectacles de lumière directement projetée sur leurs surfaces planes. La figure~\ref{figC1S2Tryphon} illustre l'un de ces tryphons. Ils peuvent être pilotés de façon télécommandée ou de façon autonome. Cependant, pour flotter en groupe de manière autonome, ces cubes aériens doivent bénéficier d'un système de localisation relative. La localisation dans ce type de situation devient un problème d'envergure. En effet, l'utilisation de caméras positionnées dans l'aire de vol devient impensable, car cette dernière peut être d'au-delà de 100 mètres de côté. L'équipement de positionnement qui est utilisé dans ces situations est trop onéreux pour un projet artistique. De plus, comme ce projet offre des représentations un peu partout sur la planète, il est parfois irréalisable d'installer un tel équipement pour localiser les robots. Il est également impossible d'utiliser de l'équipement lourd directement sur les tryphons, car leur charge utile n'est que de quelques centaines de grammes.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.8\textwidth]{../Pictures/Tryphon.png}
  \end{tabular}
 \end{center}
 \caption[Image d'un tryphon]{Image d'un tryphon. L'image est tirée de Sharf, Persson, St-Onge et Reeves~\cite{sharf2012}.}
 \label{figC1S2Tryphon}
\end{figure}

D'un point de vue technologique, le problème de localisation coopérative en temps réel, à faible coût et à faible poids représente un défi. Giguère, Rekleitis et Latulippe~\cite{giguere2012see} décrivent une méthode de localisation coopérative qui n'utilise que les mesures angulaires relevées par des robots s'observant mutuellement. Leurs travaux offrent une solution analytique de grande précision pour le problème en deux dimensions et il est possible d'étendre la validité de cette solution à l'environnement tridimensionnel. L'utilisation des caméras est envisageable puisque ces capteurs sont efficaces pour mesurer des angles et peuvent être légers et abordables. Aussi, les récentes avancées en filtrage probabiliste peuvent améliorer davantage cette solution en permettant la localisation supérieure à la précision maximale des capteurs de deux robots éloignés. De surcroît, le développement de cette technologie contribue à démocratiser la robotique mobile en rendant la résolution de l'une de ses difficultés inhérentes plus abordables à surmonter.

\subsection{Objectifs}

L'objectif de la présente maîtrise est de spécifier, de développer et de valider un système de localisation relative en temps réel de multiples robots volant à six degrés de liberté. Le système est constitué de caméras et de marqueurs lumineux pouvant être installés sur des robots volants afin d'effectuer des calculs de localisation. Les tryphons étant des robots sur lesquels des jeux de lumière prennent place, les marqueurs lumineux du système de localisation doivent paraître dans le spectre de la lumière infrarouge. Également, l'utilisation des tryphons implique que le poids d'un tel système ne peut être de plus de 200 grammes. Ce système doit enfin être robuste aux différents environnements de vols et avoir une précision de l'ordre du décimètre à de grandes distances.


Pour atteindre pleinement ces objectifs, nous devons donc :
\begin{itemize} 
\item Développer un système de traitement d'images afin de permettre la détection de marqueurs lumineux robustes aux bruits de l'environnement, en contrôlant divers paramètres d'exposition des caméras utilisées. 
\item Implémenter un programme donnant au système la faculté de calculer le centre de masse respectif de chaque marqueur lumineux.
\item Concevoir un algorithme de localisation relative en trois dimensions. Celui-ci doit être basé sur l'utilisation de caméras et de marqueurs lumineux.
\item Étudier et mettre en application différentes techniques de filtrage probabiliste telles que EKF et UKF pour faire le suivi de trajectoires.
\item Tester les différentes composantes technologiques pour créer un système pesant moins de $200~g$ qui résoud le problème de localisation à six degrés de liberté dans un environnement de déploiement artistique réaliste.
\end{itemize}


\subsection{Méthodologie}

La méthodologie adoptée pour ce problème est basée sur des tests de différentes techniques de suivi de trajectoire et de traitement d'images. Pour réaliser pleinement ce projet, nous avons d'abord effectué des tests en simulation d'un système de localisation relative impliquant deux caméras mutuellement observables et des marqueurs lumineux. Il a fallu ensuite procéder à des expérimentations de suivi de trajectoire à courte et à longue portée. L'analyse des performances de notre système devait être faite sur la base d'une comparaison avec un système de positionnement de qualité industrielle. Puis, des simulations ont été produites pour démontrer l'efficacité des filtres probabilistes développés et intégrés à notre système pour améliorer la traque des robots. Enfin, nous avons effectué des essais avec de l'équipement léger fonctionnant à l'infrarouge et pouvant être utilisé avec précision en temps réel.
