\section{Description du filtre étendu de Kalman}
\label{Chap3Sec2EKF}

La section précédente décrit le filtre de Kalman basé sur la transformation non parfumée (\emph{UKF}) utilisé pour estimer avec précision la position relative d'un agent lorsque celui-ci est à la limite de la précision des capteurs employés. Afin de pouvoir complètement estimer la pose relative de cet agent, nous devons également filtrer les mesures d'orientation relative. 

\subsection{Éléments de l'algèbre des quaternions}
\label{C3S2SS1}
Pour représenter l'orientation d'un agent dans l'espace tridimensionnel, l'emploi des quaternions s'avère être une technique efficace. Notamment, Trawny et Roumeliotis~\cite{trawny2005indirect} décrivent dans leur article un filtre étendu de Kalman (\emph{EKF}) basé sur les quaternions à cet effet. Dans la présente sous-section, nous définissons les mathématiques formant une base de connaissances nécessaire à l'utilisation des quaternions. Il est également à noter que nous utilisons les prochaines définitions comme convention pour nos quaternions, qui sont les mêmes que pour les auteurs ici mentionnés. Cette convention résulte en un système rotationnel de matrices basé sur la règle de la main gauche au lieu de la plus fréquente règle de la main droite. Il faut tenir compte de ce fait lorsque nous effectuons des conversions entre des quaternions et notre technique de localisation du chapitre~\ref{Chapitre2}.

Tout d'abord, le quaternion$ \vec{q}$ est défini par 
\begin{equation}
\vec{q} = q_4 + q_1\mathbf{i} + q_2\mathbf{j} + q_3\mathbf{k}
\end{equation}
où $\mathbf{i}$, $\mathbf{j}$ et $\mathbf{k}$ sont des nombres imaginaires satisfaisant les contraintes suivantes~:
\begin{equation}
\label{c3s2imaginary}
\mathbf{i}^2 = -1\,\, , \,\,\mathbf{j}^2 = -1\,\, , \,\, \mathbf{k}^2 = -1 \,\, , \,\, -\mathbf{i}\mathbf{j} = \mathbf{j}\mathbf{i} = \mathbf{k} \,\, , \,\, 
-\mathbf{j}\mathbf{k} = \mathbf{k}\mathbf{j} = \mathbf{i} \,\, , \,\, -\mathbf{k}\mathbf{i} = \mathbf{i}\mathbf{k} = \mathbf{j}
\end{equation}

La quantité $q_4$ est la partie scalaire du quaternion, tandis que les trois autres termes correspondent à la partie imaginaire de celui-ci. Également, nous pouvons définir ce même quaternion $\vec{q}$ par un vecteur de quatre éléments 
\begin{equation}
\label{c3s2quaternion}
\vec{q} = (\vec{q_u} \,\, q_4)^T = (q_1 \,\, q_2 \,\, q_3 \,\, q_4)^T
\end{equation}
où les composantes $\vec{q_u}$ et $q_4$ symbolisent respectivement~:
\begin{equation}
\label{c3s2q123}
\vec{q_u} = \begin{bmatrix}
k_x \sin(\theta/2) \\
k_y \sin(\theta/2) \\
k_z \sin(\theta/2)
\end{bmatrix}
\end{equation}
et
\begin{equation}
\label{c3s2q4}
\vec{q_4} = \cos(\theta/2).
\end{equation}
Ici, le vecteur $\vec{k} = [\vec{k_x \,\, k_y \,\, k_z}]^T$ est unitaire et représente un axe de rotation, tandis que $\theta$ est l'angle de cette rotation. Cette façon de représenter les rotations est beaucoup plus concise que l'utilisation d'une matrice de rotation. Ainsi, employer les quaternions permet une diminution du temps de calcul nécessaire pour effectuer des rotations.

Dans notre filtre EKF, il nous est souhaitable d'utiliser comme mesures $\vec{z_1}$ les rotations axe-angle calculées par notre technique de localisation relative. Il nous faut pouvoir convertir les quaternions en vecteurs axe-angle et inversement.  Nous obtenons une notation axe-angle $\vec{R_{aa}}$ à partir de $\vec{q}$ grâce à l'équation~\ref{c3s2convertQtoAA}. 
\begin{equation}
\label{c3s2convertQtoAA}
\vec{R_{aa}} = \begin{bmatrix}
\vec{k_x} & \vec{k_y} & \vec{k_z} & \theta
\end{bmatrix}^T
\end{equation}
L'opération inverse est illustrée par l'0~\ref{c3s2convertAAtoQ}.
\begin{equation}
\label{c3s2convertAAtoQ}
\vec{q} = \begin{bmatrix}
\vec{R_{aa}}_1 \sin\left(\frac{\vec{R_{aa}}_4}{2}\right) \\
\vec{R_{aa}}_2 \sin\left(\frac{\vec{R_{aa}}_4}{2}\right) \\
\vec{R_{aa}}_3 \sin\left(\frac{\vec{R_{aa}}_4}{2}\right) \\
\cos(\frac{\vec{R_{aa}}_4}{2})
\end{bmatrix}
\end{equation}

Pour déterminer la rotation opposée à un certain quaternion $\vec{q}$, un quaternion $\vec{q}\,^{-1}$ doit être calculé. En suivant la notation de Trawny et Roumeliotis~\cite{trawny2005indirect}, la rotation opposée se définit par l'inversion de $\vec{q}$ à l'équation~\ref{c3s2invertQ}. Cette opération correspond à l'inversion de l'axe de rotation $\vec{k}$.
\begin{equation}
\label{c3s2invertQ}
\vec{q}\,^{-1} = \begin{bmatrix}
-\vec{q_1} & -\vec{q_2} & -\vec{q_3} & \vec{q_4}
\end{bmatrix}^T
\end{equation}

Pour combiner des rotations en utilisant avec la notation des quaternions, les auteurs montrent qu'il faut effectuer ce qu'ils appellent la multiplication de quaternions. Cela correspond à un produit scalaire de deux vecteurs contenant les éléments des quaternions. Par exemple, considérons les quaternions $\vec{q}$ et $\vec{p}$. La multiplication de ces quaternions s'effectue comme suit~:
\begin{equation}
\vec{q} \otimes \vec{p} \equiv \vec{q} \cdot \vec{p} = (q_4 + q_1\mathbf{i} + q_2\mathbf{j} + q_3\mathbf{k})(p_4 + p_1\mathbf{i} + p_2\mathbf{j} + p_3\mathbf{k}).
\end{equation}

L'équation ci-dessus peut être résolue en utilisant les contraintes des nombres imaginaires mentionnées à l'équation~\ref{c3s2imaginary}. Nous obtenons ainsi le vecteur suivant~:
\begin{equation}
\label{c3s2quatmult}
\vec{q} \otimes \vec{p} = \begin{bmatrix}
 q_4p_1 + q_3p_2 - q_2p_3 + q_1p_4 \\
-q_3p_1 + q_4p_2 + q_1p_3 + q_2p_4 \\
 q_2p_1 - q_1p_2 + q_4p_3 + q_3p_4 \\
-q_1p_1 - q_2p_2 - q_3p_3 + q_4p_4
\end{bmatrix}
\end{equation}

Afin de multiplier des quaternions sous leur forme matricielle, le produit croisé doit être défini. Trawny et Roumeliotis~\cite{trawny2005indirect} introduisent l'opérateur matriciel antisymétrique $\lfloor \mathbf{q} \rfloor$ d'un quaternion $\vec{q}$. Cet opérateur est utilisé pour combiner des quaternions avec diverses matrices, ce qui survient fréquemment dans les filtres probabilistes basés sur des états exprimés en quaternions. 
\begin{equation}
\label{c3s2skew}
\lfloor \mathbf{q} \rfloor = \begin{bmatrix}
0 & -q_3 & q_2 \\
q_3 & 0 & -q_1 \\
-q_2 & q_1 & 0
\end{bmatrix}
\end{equation}

Également, pour faire le produit d'un vecteur~$\vec{\omega} = [\omega_x \,\, \omega_y \,\, \omega_z]$ avec d'un quaternion~$\vec{q}$, un opérateur matriciel $\mathbf{\Omega}(\vec{\omega})$ est utilisé. Entre autres, $\mathbf{\Omega}(\vec{\omega})$ permet de combiner une vitesse de rotation tridimensionnelle $\vec{\omega}$ avec une orientation $\vec{q}$ de départ afin de mettre à jour cette dernière. Nous pouvons ainsi utiliser cette stratégie pour prédire le changement d'état dans un filtre probabiliste où l'état représente l'orientation d'un agent. L'équation~\ref{c3s2Omega} décrit la manière de calculer l'opérateur $\mathbf{\Omega}(\vec{\omega})$.
\begin{equation}
\label{c3s2Omega}
\mathbf{\Omega}(\vec{\omega}) = \begin{bmatrix}
0    &  w_3 & -w_2 & w_1 \\
-w_3 &  0   &  w_1 & w_2 \\
w_2  & -w_1 &  0   & w_3 \\
-w_1 & -w_2 & -w_3 & 0
\end{bmatrix}
\end{equation}



\subsection{Description de l'algorithme}
\label{C3S2SS2}

Le filtre étendu de Kalman (\emph{EKF}) est une variante du filtre de Kalman remplaçant les prédictions linéaires par des généralisations non linéaires. Tout comme pour le filtre UKF présenté à la section~\ref{Chap3Sec1UKF}, le filtre EKF estime les mouvements d'un agent par une fonction non linéaire $g$ et les mesures des capteurs par une fonction non linéaire $h$. Cependant, le filtre EKF effectue une linéarisation de ces deux fonctions grâce à une expansion de Taylor. En d'autres termes, la fonction non linéaire $g$ est approximée selon l'état moyen précédent $\vec{\mu_0}$ et selon la commande $\vec{u_1}$. $g$ est ensuite utilisé pour prédire l'état moyen courant $\vec{\mu^p_1}$. Pour propager l'erreur lors de la transition d'état, le filtre utilise les séries de Taylor pour construire une jacobienne, c.-à-d. une matrice de gradient de $g$. Le même procédé est employé pour intégrer le bruit des mesures $\vec{z_1}$ lors de l'estimation de l'état. La figure ~\ref{figC3S2EKFLinearisation} résume le fonctionnement du filtre EKF.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=0.2cm 0.2cm 0.2cm 0.2cm, clip=true, width=0.65\textwidth]{../Figures/C3S2EKFLinearisation.png}
  \end{tabular}
 \end{center}
 \caption[Linéarisation du filtre EKF]{Linéarisation du filtre EKF. Version modifiée de Van Der Merwe~\cite{van2004sigma}}
 \label{figC3S2EKFLinearisation}
\end{figure}

Thrun, Burgard et Fox~\cite{thrun2005probabilistic} présentent dans leur livre l'algorithme du filtre EKF et font une dérivation mathématique complète de celui-ci. Les auteurs expliquent également que la force de ce filtre est sa simplicité et son efficience en temps de calcul. Cependant, les auteurs mentionnent que le filtre EKF, bien que supérieur au filtre de Kalman, possède quelques faiblesses. Entre autres, la précision du filtre est dépendante de l'incertitude de l'état précédent, car une plus grande incertitude peut mener à des estimations moins précises. Par ailleurs, si les fonctions non linéaires $g$ et $h$ sont fortement non linéaires aux alentours des moyennes respectives $\vec{\mu_0}$ et $\vec{z_1}$, le filtre EKF peut retourner des estimations avec de grandes erreurs d'approximations. De surcroît, Thrun, Burgard et Fox ont montré que la technique de linéarisation du filtre UKF était plus performante que la linéarisation par expansion de Taylor du filtre EKF. 

Néanmoins, malgré ces critiques, nous choisissons d'utiliser le filtre EKF pour améliorer l'estimation de l'orientation déterminée par notre technique de localisation. Quelques raisons motivent notre décision. D'abord, nous avons développé notre technique de localisation relative pour les tryphons~\cite{sharf2012}. Ces cubes flottants possèdent une bonne inertie, ce qui rend leurs mouvements très lents. Par conséquent, si nous considérons que l'intervalle de temps $\Delta_t = t_1 - t_0$ entre chaque exécution du filtre est court, nous pouvons considérer que $g$ est presque linéaire. Ensuite, sur de longues distances, notre technique de localisation affiche une précision de $0.0238\,rad$ en moyenne sur l'estimation de l'orientation relative des agents. Puisque nous utilisons ces estimations en tant que mesures $\vec{z_1}$ dans notre filtre EKF, nous estimons que le filtre EKF est employé dans des conditions optimales. Enfin, la notation des quaternions utilisée par Trawny et Roumeliotis~\cite{trawny2005indirect} est exhaustive et la variante du filtre EKF que développent ceux-ci correspond à peu d'éléments près à celle dont nous avons besoin. 

Définissons le problème auquel s'attaque le filtre EKF que nous utilisons. Nous avons un agent pouvant pivoter autour de lui-même dans l'espace tridimensionnel. L'agent possède un capteur gyroscopique calibré à trois axes lui permettant de déterminer sa vitesse rotationnelle dans le temps. Dans la réalité, les gyroscopes possèdent un biais qui affecte ses mesures. Par conséquent, nous devons considérer celui-ci dans notre filtre. De plus, notre agent bénéficie de notre technique de localisation relative pour estimer son orientation à un moment précis.

Soit les paramètres suivants, qui sont envoyés à l'entrée du filtre EKF. Nous définissons le vecteur $\vec{\mu_0}$ en tant qu'état initial de notre agent~:
\begin{equation}
\vec{\mu_0} = [\vec{q^\mu_0} \,\, \vec{b_0}]^T
\end{equation}
où $\vec{q^\mu_0}$ est le quaternion représentant l'orientation estimée précédente de l'agent et où $\vec{b_0}$ est un vecteur illustrant le biais de notre capteur gyroscopique. Nous avons également la matrice de covariance $\mathbf{\Sigma_0}$, qui contient l'incertitude sur l'estimation de l'état $\vec{\mu_0}$. Comme tous les filtres probabilistes, nous recevons en entrée la commande de transition de l'état, $\vec{u_1}$. Dans cette variante du filtre EKF, $\vec{u_1}$ est une matrice contenant les mesures gyroscopiques $\vec{\omega_0}$ et $\vec{\omega_1}$ capturées au temps $t_0$ et $t_1$, respectivement. Évidemment, nous connaissons l'intervalle de temps $\Delta_t$ entre $t_0$ et $t_1$. De plus, puisque le gyroscope est calibré, nous sommes capables de connaître le bruit sur ses mesures. Ce bruit est découpé en deux entités indépendantes. La première composante est un bruit blanc gaussien $\sigma_r$ qui affecte purement les mesures de vitesses rotationnelles. La seconde composante est un bruit blanc gaussien non statique, noté $\sigma_b$, qui fait varier dans le temps le biais sur les mesures du gyroscope. 

Les derniers paramètres que nous recevons à l'entrée du filtre EKF sont la mesure $\vec{z_1}$ et sa matrice de covariance $\mathbf{\Sigma_z}$. Dans notre algorithme, $\vec{z_1}$ est l'orientation de l'agent sous la notation axe-angle, telle que mesurée par notre technique de localisation relative présentée au chapitre~\ref{Chapitre2}. Quant à la matrice $\mathbf{\Sigma_z}$, nous la définissons comme une surestimation de l'incertitude sur l'orientation calculée par notre technique basée sur les résultats expérimentaux détaillés à la section~\ref{Chap2Sec3EXP}. Nous procédons ainsi puisque nous ne connaissons pas avec certitude l'effet sur la mesure $\vec{z_1}$ de la propagation des erreurs provenant de la précision des caméras et de l'estimation de l'angle~$\beta$. Nous invitons le lecteur à se référer à la sous-section~\ref{C2S2SS2} pour des explications concernant l'estimation de l'angle~$\beta$.

L'algorithme EKF est implémenté comme suit. Tout d'abord, nous pouvons utiliser la vitesse rotationnelle $\vec{\omega_0}$ mesurée et le biais précédent $\vec{b_0}$ pour estimer la vitesse rotationnelle non biaisée précédente $\vec{\omega^p_0}$.
\begin{equation}
\vec{\omega^p_0} = \vec{\omega_0} - \vec{b_0}
\end{equation}
Afin de déterminer la vitesse rotationnelle non biaisée actuelle, nous devons propager le biais $\vec{b_0}$ pour prédire le biais courant $\vec{b^p_1}$. Pour ce faire, nous posons l'hypothèse que le biais demeure constant dans l'intervalle d'intégration $\Delta_t$. Cela est vrai pour un petit intervalle de temps et nous n'avons ainsi qu'à utiliser $\sigma_b$ lors de nos estimations pour tenir compte de cette approximation.
\begin{equation}
\label{c3s2eqConstBias}
\vec{b^p_1} = \vec{b_0}
\end{equation}
Ainsi, nous sommes en mesure d'estimer la nouvelle vitesse rotationnelle non biaisée $\vec{\omega^p_1}$.
\begin{equation}
\vec{\omega^p_1} = \vec{\omega_1} - \vec{b^p_1}
\end{equation}

À partir des vitesses $\vec{\omega^p_0}$ et $\vec{\omega^p_1}$, nous calculons la propagation du quaternion de l'état $\vec{q^\mu_0}$ vers $\vec{q^p_1}$, la prédiction de son état actuel, grâce à une intégration de premier ordre. En d'autres termes, nous supposons l'évolution linéaire de la vitesse rotationnelle $\omega$ durant l'intervalle $\Delta_t$. De cette façon, nous pouvons calculer $\vec{\omega^p_\mu}$, qui est la moyenne entre $\vec{\omega^p_0}$ et $\vec{\omega^p_1}$, et l'utiliser pour prédire $\vec{q^p_1}$.
\begin{equation}
\vec{\omega^p_\mu} = \frac{\vec{\omega^p_0} + \vec{\omega^p_1}}{2}
\end{equation}

La propagation vers $\vec{q^p_1}$ est défini à l'équation~\ref{c3s2qmu1}. Trawny et Roumeliotis~\cite{trawny2005indirect} démontrent cette équation en illustrant que la manière de propager un quaternion résulte en une expansion de séries de Taylor et en montrant que les dérivés des matrices~$\mathbf{\Omega}$ donnent zéro.
\begin{equation}
\label{c3s2qmu1}
\vec{q^p_1} = \left( \exp\left( \frac{\mathbf{\Omega}(\vec{\omega^p_\mu})\Delta_t}{2} \right) + 
       \frac{1}{48}\left(\mathbf{\Omega}(\vec{\omega^p_1})\mathbf{\Omega}(\vec{\omega^p_0}) - \mathbf{\Omega}(\vec{\omega^p_0})\mathbf{\Omega}(\vec{\omega^p_1}) \right)\Delta_t^2 \right)\vec{q^\mu_0}
\end{equation}
Après avoir complété la propagation vers $\vec{q^p_1}$, nous devons nous assurer que ce quaternion est toujours un vecteur unitaire.
\begin{equation}
\vec{q^p_1} = \frac{\vec{q^p_1}}{| \vec{q^p_1} |}
\end{equation}

Dans l'algorithme EKF présenté par Thrun, Burgard et Fox~\cite{thrun2005probabilistic}, l'étape suivant la prédiction de l'état $\vec{\mu^p_1}$ est la propagation de l'erreur sur l'état afin d'obtenir la matrice de covariance $\mathbf{\Sigma^p_1}$. Ici, nous avons l'état prédit $\vec{\mu^p_1}$ comme suit~:
\begin{equation}
\vec{\mu^p_1} = [\vec{q^p_1} \,\, \vec{b^p_1}]^T.
\end{equation}
Afin de calculer la propagation vers $\mathbf{\Sigma^p_1}$, nous devons calculer deux matrices de transition d'état, soit la matrice de transition d'état $\mathbf{\Phi}$ et $\mathbf{Q_d}$ la matrice de covariance du bruit sur la commande $\vec{u_1}$. L'équation~\ref{c3s2phi}, tirée de Trawny et Roumeliotis~\cite{trawny2005indirect}, permet d'évaluer $\mathbf{\Phi}$.
\begin{equation}
\label{c3s2phi}
\mathbf{\Phi} = \begin{bmatrix}
\mathbf{\Theta} & \mathbf{\Psi} \\
\mathbf{0} & \mathbf{1}
\end{bmatrix}
\end{equation}

Dans l'équation~\ref{c3s2phi}, $\mathbf{0}$ et $\mathbf{I}$ sont respectivement une matrice de zéros et la matrice identité. La matrice $\mathbf{\Theta}$ se définit par l'équation~\ref{c3s2theta}. Cependant, pour de petites valeurs $|\vec{\omega^p_\mu}|$, les divisions peuvent mener à de l'instabilité numérique. Par conséquent, dans ces situations, l'équation~\ref{c3s2thetaApprox}, qui s'obtient à l'aide la règle de L'Hôpital, permet d'estimer la matrice $\mathbf{\Theta}$.
\begin{equation}
\label{c3s2theta}
\mathbf{\Theta} = \cos\left(|\vec{\omega^p_\mu}| \Delta_t\right)\mathbf{I} - \sin\left(|\vec{\omega^p_\mu}| \Delta_t\right) \lfloor \frac{\vec{\omega^p_\mu}}{|\vec{\omega^p_\mu}|} \rfloor
    + \left(1-\cos(|\vec{\omega^p_\mu}| \Delta_t)\right)\frac{\vec{\omega^p_\mu}}{|\vec{\omega^p_\mu}|} \frac{\vec{\omega^p_\mu}^T}{|\vec{\omega^p_\mu}|}
\end{equation}
\begin{equation}
\label{c3s2thetaApprox}
\mathbf{\Theta} = \mathbf{I} - \Delta_t \lfloor\vec{\omega^p_\mu}\rfloor + \frac{\Delta_t^2}{2} \lfloor\vec{\omega^p_\mu}\rfloor^2
\end{equation}

Similairement, la matrice $\mathbf{\Psi}$ se détermine par l'équation~\ref{c3s2psi}. Encore une fois, pour de petites valeurs $|\vec{\omega^p_\mu}|$, les divisions peuvent mener à de l'instabilité numérique. Donc, l'équation~\ref{c3s2psiApprox} propose une technique estimant $\mathbf{\Psi}$ à l'aide de la règle de L'Hôpital.
\begin{equation}
\label{c3s2psi}
\mathbf{\Psi} = -\mathbf{I}\Delta_t + \frac{1}{|\vec{\omega^p_\mu}|^2}\left(1-\cos(|\vec{\omega^p_\mu}| \Delta_t)\right)\lfloor \vec{\omega^p_\mu} \rfloor 
 - \frac{1}{|\vec{\omega^p_\mu}|^3}\left(|\vec{\omega^p_\mu}| \Delta_t - \sin(|\vec{\omega^p_\mu}| \Delta_t)\right)\lfloor \vec{\omega^p_\mu} \rfloor^2
\end{equation}
\begin{equation}
\label{c3s2psiApprox}
\mathbf{\Psi} = -\mathbf{I}\Delta_t + \frac{\Delta_t^2}{2}\lfloor \vec{\omega^p_\mu} \rfloor -  \frac{\Delta_t^3}{6}\lfloor \vec{\omega^p_\mu} \rfloor^2
\end{equation}

Quant à elle, la matrice $\mathbf{Q_d}$ a vu sa construction détaillée par Trawny et Roumeliotis~\cite{trawny2005indirect}. Dans un système de temps discret, les auteurs montrent dans leur article que $\mathbf{Q_d}$, la matrice de covariance du bruit, est de la forme suivante.
\begin{equation}
\label{c3s2qd}
\mathbf{Q_d} = \begin{bmatrix}
\mathbf{Q_{11}} & \mathbf{Q_{12}} \\
\mathbf{Q_{12}}^T & \mathbf{Q_{22}}
\end{bmatrix}
\end{equation}

Les équations \ref{c3s2q11}, \ref{c3s2q12} et \ref{c3s2q22} représentent respectivement les détails des matrices $\mathbf{Q_{11}}$, $\mathbf{Q_{12}}$ et $\mathbf{Q_{22}}$. Puisque $\mathbf{Q_{11}}$ et $\mathbf{Q_{12}}$ font usage de divisions par $|\vec{\omega^p_\mu}|$, la règle de L'Hôpital est utilisée pour estimer ces matrices. L'estimation de $\mathbf{Q_{11}}$ se trouve à l'équation~\ref{c3s2q11approx} et celle de $\mathbf{Q_{12}}$ se trouve à l'équation~\ref{c3s2q12approx}.
\begin{equation}
\label{c3s2q11}
\mathbf{Q_{11}} = \sigma^2_r \Delta_t \mathbf{I} + \sigma^2_w \left( \mathbf{I} \frac{\Delta_t^3}{3} + \frac{\frac{(|\vec{\omega^p_\mu}|\Delta_t)^3}{3} 
+ 2\sin(|\vec{\omega^p_\mu}|\Delta_t) - 2|\vec{\omega^p_\mu}|\Delta_t}{|\vec{\omega^p_\mu}|^5} \lfloor \vec{\omega^p_\mu} \rfloor^2 \right)
\end{equation}
\begin{equation}
\label{c3s2q11approx}
\mathbf{Q_{11}} = \sigma^2_r \Delta_t \mathbf{I} + \sigma^2_w \left( \mathbf{I} \frac{\Delta_t^3}{3} + \frac{2\Delta_t^5}{5!} \lfloor \vec{\omega^p_\mu} \rfloor^2 \right)
\end{equation}
\begin{equation}
\label{c3s2q12}
\mathbf{Q_{12}} = -\sigma^2_w \left( \mathbf{I} \frac{\Delta_t^2}{2} - \frac{|\vec{\omega^p_\mu}|\Delta_t - \sin(|\vec{\omega^p_\mu}|\Delta_t)}{|\vec{\omega^p_\mu}|^3} \lfloor \vec{\omega^p_\mu} \rfloor 
+ \frac{\frac{(|\vec{\omega^p_\mu}|\Delta_t)^2}{2} + \cos(|\vec{\omega^p_\mu}|\Delta_t) -1}{|\vec{\omega^p_\mu}|^4}   \lfloor \vec{\omega^p_\mu} \rfloor^2  \right)
\end{equation}
\begin{equation}
\label{c3s2q12approx}
\mathbf{Q_{12}} = -\sigma^2_w \left( \mathbf{I} \frac{\Delta_t^2}{2} - \frac{\Delta_t^3}{3!} \lfloor \vec{\omega^p_\mu} \rfloor + \frac{\Delta_t^4}{4!} \lfloor \vec{\omega^p_\mu} \rfloor^2  \right)
\end{equation}
\begin{equation}
\label{c3s2q22}
\mathbf{Q_{12}} = \sigma_w^2 \Delta_t \mathbf{I}
\end{equation}

À partir des matrices $\mathbf{\Phi}$ et $\mathbf{Q_d}$, nous pouvons par la suite calculer la matrice de covariance $\mathbf{\Sigma^p_1}$ avec l'équation~\ref{c3s2sigmaP1}.
\begin{equation}
\label{c3s2sigmaP1}
\mathbf{\Sigma^p_1} = \mathbf{\Phi} \mathbf{\Sigma^p_0} \mathbf{\Phi}^T + \mathbf{Q_d}
\end{equation}

Selon l'algorithme EKF, nous devons par la suite calculer la matrice du gain de Kalman, $\mathbf{K}$. Cependant, notre variante implique que nous devons auparavant déterminer quelques éléments. Tout d'abord, nous devons utiliser $\vec{\mu^p_1}$ afin de prédire la mesure $\vec{z^p_1}$. Dans notre situation, la mesure n'est pas associée au biais $\vec{b^p_1}$ puisque sa présence dans l'état $\vec{\mu^p_1}$ ne permet que de suivre la progression du biais dans les capteurs gyroscopiques. Par conséquent, nous utilisons $\vec{q^p_1}$ pour prédire $\vec{z^p_1}$. Également, l'utilité de la prédiction de la mesure $\vec{z^p_1}$ est de comparer celle-ci à la vraie mesure $\vec{z_1}$. Puisque notre véritable mesure $\vec{z_1}$ est une orientation sur la forme axe-angle, nous allons plutôt convertir cette dernière, en utilisant l'équation~\ref{c3s2convertAAtoQ}, pour obtenir le quaternion $\vec{z^q_1}$. Ainsi, nous pouvons calculer la différence $\vec{r}$ de rotation entre les deux quaternions $\vec{z^q_1}$ et $\vec{z^p_1}$ en posant~:
\begin{equation}
\vec{z^p_1} = \vec{q^p_1}
\end{equation}
et en utilisant les équations~\ref{c3s2invertQ} et~\ref{c3s2quatmult} à notre avantage.
\begin{equation}
\vec{r} = \vec{z_1} \otimes \vec{z^p_1}\,^{-1}
\end{equation}

Nous devons ensuite déterminer la matrice de mesure~$\mathbf{H}$ pour combiner la covariance $\mathbf{\Sigma^p_1}$ à l'erreur $\mathbf{\Sigma_z}$ de nos mesures. Étant donné que nous convertissons le quaternion $\vec{z^q_1}$ comme mesure comparée à notre prédiction $\vec{z^p_1}$, et puisque le biais du gyroscope n'est pas mesuré par $\vec{z_1}$, la matrice $\mathbf{H}$ se définit par l'équation~\ref{c3s2H}.
\begin{equation}
\label{c3s2H}
\mathbf{H} = [\lfloor \vec{z^q_1} \rfloor \,\, \mathbf{0}]
\end{equation}
Grâce à $\mathbf{H}$, nous sommes en mesure de calculer $\mathbf{S}$, la matrice de covariance sommative du système.
\begin{equation}
\label{c3s2S}
\mathbf{S} = \mathbf{H} \,\, \mathbf{\Sigma^p_1} \,\, \mathbf{H}^{-1} + \mathbf{\Sigma_z}
\end{equation}

Ainsi, il est possible de calculer la matrice du gain de Kalman, $\mathbf{K}$, en employant l'équation~\ref{c3s2K}. Cette matrice nous permet de faire une estimation de $\vec{\mu_1}$ et de son erreur $\mathbf{\Sigma_1}$ en intégrant la mesure $\vec{z_1}$ dans le système.
\begin{equation}
\label{c3s2K}
\mathbf{K} = \mathbf{\Sigma^p_1} \,\, \mathbf{H} \,\, \mathbf{S}^{-1}
\end{equation}

D'après la variante du filtre EKF présentée par Trawny et Roumeliotis~\cite{trawny2005indirect}, nous pouvons utiliser $\mathbf{K}$ pour déterminer $\vec{\delta_\mu}$, le vecteur de correction de l'état $\vec{\mu_0}$. Comme le présentent ces auteurs, $\vec{\delta_\mu}$ se calcule à l'aide de l'équation~\ref{c3s2deltamu}.
\begin{equation}
\label{c3s2deltamu}
\vec{\delta_\mu} = \mathbf{K}\vec{r}
\end{equation}
Ici cependant, notre implémentation implique que $\vec{r}$ est un quaternion. Afin de permettre la correspondance des dimensions et ainsi rendre l'équation~\ref{c3s2deltamu} possible, nous utilisons les trois premiers éléments du vecteur $\vec{r}$. Cette technique ne fait pas perdre d'information, comme le montre la définition du quaternion à l'équation~\ref{c3s2quaternion}. C'est d'ailleurs la stratégie même qui est utilisée pour calculer la matrice $\lfloor \mathbf{q} \rfloor$ à l'équation~\ref{c3s2skew}.

Le résultat de l'équation~\ref{c3s2deltamu}, $\vec{\delta_\mu}$, est composé de deux vecteurs, $\vec{\delta_q}$ et $\vec{\delta_b}$, qui sont respectivement les corrections à appliquer à l'orientation prédite $\vec{q^p_1}$ et au biais $\vec{b^p_1}$.
\begin{equation}
\vec{\delta_\mu} = \begin{bmatrix}
2\vec{\delta_q} \\
\vec{b^p_1}
\end{bmatrix}
\end{equation}

Utilisons donc $\vec{\delta_q}$ pour corriger notre prédiction $\vec{q^p_1}$. Pour ce faire, nous allons considérer $\vec{\delta^\mu_q}$, le quaternion qui sert à appliquer la correction.
\begin{equation}
\vec{\delta^\mu_q} = \begin{bmatrix}
\vec{\delta_q} \\
\sqrt{1 - \vec{\delta_q}^T\vec{\delta_q}}
\end{bmatrix}
\end{equation} 
Advenant le cas où $\vec{\delta_q}^T\vec{\delta_q} = -1$, nous pouvons également calculer $\vec{\delta^\mu_q}$ comme suit.
\begin{equation}
\vec{\delta^\mu_q} = \frac{1}{\sqrt{1 + \vec{\delta_q}^T\vec{\delta_q}}} \begin{bmatrix}
\vec{\delta_q} \\
1
\end{bmatrix}
\end{equation}
Ainsi, $\vec{q^p_1}$ est corrigé par l'équation~\ref{c3s2qmu1} pour obtenir l'estimation du quaternion de l'état actuel, $\vec{q^\mu_1}$.
\begin{equation}
\vec{q^\mu_1} = \vec{\delta^\mu_q} \otimes \vec{q^p_1}
\end{equation}
De la même manière, $\vec{\delta_b}$ est utilisé pour corriger la prédiction du biais $\vec{b^p_1}$ afin d'obtenir le nouveau biais, $\vec{b_1}$.
\begin{equation}
\vec{b_1} = \vec{b^p_1} + \vec{\delta_b}
\end{equation}
Il est à noter que ce biais corrigé $\vec{b_1}$ permet de corriger la mesure gyroscopique $\vec{\omega_1}$ utilisée dans cette itération du filtre EKF. Puisqu'à l'itération suivante, $\vec{\omega_1}$ devient $\vec{\omega_0}$, il est important d'appliquer le vrai biais $\vec{b_1}$ aux mesures gyroscopiques contenues dans la commande $\vec{u_1}$ pour éviter de faire diverger l'algorithme.

Enfin, nous obtenons $\vec{\mu_1}$ par une simple combinaison de $\vec{q^\mu_1}$ et de $\vec{b_1}$, tel qu'illustré par l'équation~\ref{c3s2mu1}. $\mathbf{\Sigma_1}$, sa matrice de covariance, se calcule grâce à l'équation~\ref{c3s2Sigma1}
\begin{equation}
\label{c3s2mu1}
\vec{\mu_1} = \begin{bmatrix}
\vec{q^\mu_1} \\
\vec{b_1}
\end{bmatrix}
\end{equation}
\begin{equation}
\label{c3s2Sigma1}
\mathbf{\Sigma_1} = (\mathbf{I} - \mathbf{K}\mathbf{H}) \,\, \mathbf{\Sigma^p_1} \,\, (\mathbf{I} - \mathbf{K}\mathbf{H})^T + \mathbf{K} \mathbf{\Sigma_z} \mathbf{K}^T
\end{equation}


\subsection{Simulation et résultats}
\label{C3S2SS3}

La simulation dont nous avons discuté à la section~\ref{Chap3Sec1UKF} permet de valider l'efficacité du suivi de la position d'un tryphon, un robot dirigeable cubique présenté à la section~\ref{Chap1Seq2QHYPG}. Nous cherchons à développer un système de filtrage probabiliste pour traquer la pose de ce robot et le filtre UKF de la section~\ref{Chap3Sec1UKF} permet d'estimer la position de celui-ci. Afin de compléter ce système de filtrage, nous avons réalisé une simulation pour analyser l'efficacité du filtre EKF à faire le suivi de l'orientation d'un tryphon. Dans cette simulation, nous avons défini un tryphon comme un robot pouvant pivoter sur lui-même dans les trois dimensions. Ce robot est muni d'un équipement gyroscopique permettant d'estimer sa vitesse rotationnelle $\omega_1$ à chaque intervalle de temps $\Delta_t$, tel qu'expliqué à la section~\ref{C3S2SS2}. Dans cette simulation, nous avons deux tryphons dotés chacun d'une caméra et de marqueurs lumineux. Nous supposons que le tryphon de référence $Robot_A$ est immobile alors que le second tryphon, $Robot_B$, pivote sur lui-même. 

Nous utilisons les mesures gyroscopiques $\omega_0$ et $\omega_1$ de $Robot_B$ en tant que commande $\vec{u_1}$ en entrée dans notre filtre EKF. Comme pour la section~\ref{Chap3Sec1UKF}, nous avons modélisé les mouvements de rotation du tryphon en respectant la manière à laquelle se déplacent ces cubes flottants dans la réalité. 

Étant donné que nous ne connaissons pas le modèle dirigeant la progression du biais de $\vec{b_0}$ vers $\vec{b_1}$ dans les capteurs gyroscopiques du tryphon $Robot_B$, nous avons établi une progression aléatoire de celui-ci afin d'affecter $\omega_0$ et~$\omega_1$. Cependant, dans notre filtre EKF basé sur les quaternions, nous avons considéré que le biais $\vec{b_0}$ demeure constant, comme l'indique l'équation~\ref{c3s2eqConstBias}. 

Afin d'obtenir une mesure $\vec{z_1}$ de l'orientation axe-angle du tryphon $Robot_B$, nous rendons possible la modélisation de la technique de localisation développée au chapitre~\ref{Chapitre2}. Cependant, afin de simplifier la simulation, nous nous contentons de calculer directement le quaternion de l'orientation réelle du tryphon $Robot_B$. Nous convertissons celui-ci dans la notation axe-angle après y avoir appliqué une rotation correspondant à l'erreur $\mathbf{\Sigma_z}$. Néanmoins, nous nous assurons de surestimer l'erreur réelle sur les mesures d'orientation relatives qui ont été observées lors des expérimentations qui sont détaillées à la section~\ref{Chap2Sec3EXP}.

Le lecteur est invité à consulter l'Appendice~\ref{AppendixE}, où il peut y trouver l'implémentation de cette variante du filtre EKF basé sur les quaternions. Cette variante a été implémentée pour être utilisée dans le logiciel Matlab.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=4.4cm 0cm 4.4cm 0cm, clip=true,  width=0.97\textwidth]{../Figures/C3S2EKFResults.png}
  \end{tabular}
 \end{center}
 \caption[Simulation de l'orientation avec le filtre EKF]{Simulation de l'estimation de l'orientation avec la variante basée sur les quaternions du filtre EKF. En haut à gauche, le graphique indiquant le bruit en radians résultant de l'incertitude des mesures de l'orientation du tryphon $Robot_B$. Cette incertitude est due à la précision subpixel des caméras utilisées. En haut à droite, l'erreur sur $\vec{\mu_1}$ à la sortie du filtre EKF. En bas, l'erreur sur l'estimation de l'orientation du tryphon pour chaque nouvel échantillon.}
 \label{figC3S2EKFResults}
\end{figure}

La figure~\ref{figC3S2EKFResults} présente les résultats de notre simulation. Toutes les valeurs d'erreur sont toutes positives, car nous nous intéressons à connaître la norme de l'erreur plutôt que sa direction. Comme nous pouvons le constater, bien que les mesures sont relativement bruitées, le filtre EKF parvient à adoucir l'erreur sur $\vec{\mu_1}$, c.-à-d. les estimations de l'orientation de l'agent. Il s'agit du comportement souhaité ici, puisque nous voulons permettre aux tryphons d'améliorer la précision de leur localisation relative au-delà de la limite de résolution des caméras utilisées. Les erreurs de positionnement des estimations de $\vec{mu_1}$ sont groupées, plus faibles et de variance plus petite que les mesures $\vec{z_1}$.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=4.4cm 0cm 4.4cm 0cm, clip=true,  width=0.97\textwidth]{../Figures/C3S2EKFBias.png}
  \end{tabular}
 \end{center}
 \caption[Estimation du biais avec le filtre EKF]{Estimation du biais des capteurs gyroscopiques avec le filtre EKF. À l'équation~\ref{c3s2eqConstBias}, la prédiction du biais $\vec{b^p_1}$ s'effectue en supposant sa stabilité. $\vec{z_1}$ ne fournis pas plus d'information sur le comportement du biais. Par conséquent, l'imprécision des mesures $\vec{z_1}$ provoque une estimation aléatoire du biais $\vec{b_1}$. En rouge, on peut voir que ce dernier n'est pas en corrélation avec le biais réel, modélisé ici par une marche aléatoire.}
 \label{figC3S2EKFBias}
\end{figure}

Une difficulté avec ce filtre qui est toujours présente est l'estimation du biais $\vec{b_1}$. En effet, nous n'avons aucune valeur dans le vecteur de mesure $\vec{z_1}$ nous renseignant sur la valeur courante du biais. De surcroît, notre modélisation du biais dans le filtre EKF prédit la stabilité de celui-ci. Si la progression du biais n'est pas dirigée dans une direction fixe ou si cette progression suit une marche complètement aléatoire, cette variante du filtre EKF s'avère inefficace pour la prédiction du biais $\vec{b^p_1}$. Cela résulte en un biais estimé $\vec{b_1}$ de manière imprévisible, comme l'illustre la figure~\ref{figC3S2EKFBias}. Afin d'être applicable dans notre solution de localisation relative, une technique possible pour éliminer le problème du biais est d'évaluer ce dernier périodiquement afin de l'inclure dans les mesures $\vec{z_1}$. Il est également envisageable de calibrer les capteurs gyroscopiques afin d'estimer un certain modèle de dérivation du biais. Ce modèle peut ensuite être intégré au filtre EKF pour améliorer les performances de localisation.


