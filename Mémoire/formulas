compute alpha angle
^B\vec{P}_{R} = [ ^B\dot{P}_{Ru} \quad ^B\dot{P}_{Rv} \quad f_B]
^B\vec{P}_{L} = [ ^B\dot{P}_{Lu} \quad ^B\dot{P}_{Lv} \quad f_B]
\alpha = \arccos(\frac {^B\vec{P}_{L} \cdot ^B\vec{P}_{R}}{|^B\vec{P}_{L}||^B\vec{P}_{R}|} )


compute beta angle
^A\vec{P}_{B} = [ ^A\dot{P}_{Bu} \quad ^A\dot{P}_{Bv} \quad f_A]
\vec{R}_A is a 3D vector, from \vec{C}_A toward R_A
\beta = \frac{\pi}{2} - \arccos(\frac{^A\vec{P}_{B} \cdot \vec{R}_A } {|^A\vec{P}_{B}| |\vec{R}_A|} )


compute the distance l, knowing distance d between markers
l =\frac{d}{2 \sin\alpha} \left ({ \cos\alpha \cos\beta + \sqrt{1-\cos^2\alpha \sin^2\beta}} \right )

Compute position : a translation of l ^A\vec{P}_{B}
 \vec{T} = l \frac{^A\vec{P}_{B}}{|^A\vec{P}_{B}|}

Compute orientation
Part 1
\vec{n}_A = \vec{R}_A \times \,^A\vec{P}_{B}
\vec{n}_B = \, ^B\vec{P}_{L} \times \, ^B\vec{P}_{R}
\hat{e}_1 = (\frac{\vec{n}_B \times \vec{n}_A}{|\vec{n}_B||\vec{n}_A|})
\Theta_1 = \arccos (\frac{\vec{n}_B \cdot \vec{n}_A}{|\vec{n}_B||\vec{n}_A|})
^A_B\mathbf{R_1} = I + [\hat{e}_1]_x + (1 - \cos\Theta_1 )(\Theta_1\Theta_1^T - I)
which can be written as
^A_B\mathbf{R_1} = \mbox{Rodrigues}(\hat{e}_1,\Theta_1).

Part 2
\hat{e}_2 = \vec{n}_A.
\Theta_2 = \pi - \arccos(\frac{^A\vec{P}_{B} \cdot ^A_B\mathbf{R_1}^B\vec{P}_{A}}{|^A\vec{P}_{B}| |^B\vec{P}_{A}|})
 ^A_B\mathbf{R_2} = \mbox{Rodrigues}(\hat{e}_2,\Theta_2)
 
Final colinear formula
 \,^AF_B = ^A_B\mathbf{T}^A_B\mathbf{R}F_B' = ^A_B\mathbf{T}^A_B\mathbf{R_2}^A_B\mathbf{R_1}F_B'
 
 
 ===============
Non-colinear extension
Table explaining the effects of the offsets
\begin{center}
  \begin{tabular}{| l || c | c |}
    \hline
    Perturbation on & Camera $C_A$ & Camera $C_B$ \\ \hline
    Affects the angle & $\alpha$ & $\beta$ \\ \hline
    Alterate the estimation of & distance $l$ & position \\ \hline
    Correct $^A_B\mathbf{T}$ by & translation & rotation \\ \hline
    Correct $^A_B\mathbf{R}$ by & small rotation & small rotation \\ \hline
  \end{tabular}
\end{center}

When \vec{C}_A is not colinear
\Theta_A = \mbox{acos} (\frac{\vec{T} \cdot \vec{S}_A}{|\vec{T}||\vec{S}_A|}).
\epsilon_l = |\vec{S}_A|\cos(\Theta_A)
\vec{T}_\epsilon = \epsilon_l\frac{\vec{T}}{|\vec{T}|}
	\mathbf{R}^A_\epsilon = \mbox{Rodrigues}\left(\vec{L_AR_A}, \arcsin\left(\sin(\Theta_A)\frac{|\vec{S}_A|}{|\vec{T}|+\epsilon_l}\right)\right)
	\,^AF_B = \mathbf{T}_\epsilon \, ^A_B\mathbf{T} \mathbf{R}^A_\epsilon \, ^A_B\mathbf{R_2} \, ^A_B\mathbf{R_1} F_B'
	
When \vec{C}_B is not colinear
	\Theta_B = \arccos \left( \frac{^A_B\mathbf{R_1}^{-1}\,^A_B\mathbf{R_2}^{-1}(-\vec{T})}{|\vec{T}|} \cdot \frac{\vec{S}_B}{|\vec{S}_B|} \right)
	\epsilon_\theta = \arcsin(\sin(\Theta_B)\frac{|\vec{S}_B|}{|\vec{T}|+\epsilon_l})
	\mathbf{R}^B_\epsilon = \mbox{Rodrigues}(\vec{L_AR_A}, \epsilon_\theta)
	
	Final formula when both are not colinear, so you can deduce what is the formula when only B is not colinear
	\,^AF_B = \mathbf{R}^B_\epsilon \mathbf{T}_\epsilon \,^A_B\mathbf{T} \mathbf{R}^B_\epsilon \mathbf{R}^A_\epsilon \,^A_B\mathbf{R_2} \,^A_B\mathbf{R_1} F_B'
	
	





